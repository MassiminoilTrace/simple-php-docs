<?php namespace Template;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 

require_once __DIR__ . "/../template_load.php";

class Template extends TemplateBase
{
    // Category body
    // Should render a list of categories and pages 
    // inside the current provided category
    public function render_category(CategoryData $cat)
    {
        foreach ($cat->children as $child)
        {
            // render each child here
        }
    }

    // Render and show a page
    public function render_page_view(PageData $page)
    {
        echo $page->content_html;
    }

    // Renders the form with a textarea to
    // modify a page.
    // The form should provide:
    // - POST to $page->path->url_mut
    // - title field, with id="titolo"
    // - content textarea, with id="contenuto"
    public function render_page_edit(PageData $page)
    {
    }

    public function render_page_create(CategoryData $cat)
    {
    }

    public function list_images(PageData $page)
    {
        require __DIR__ . "/header.php";
        echo "<ul>";
        foreach ($path->get_images() as $img)
        {
            $url = $img->as_url() . "&show";
            echo "<li>" . $img->get_filename() . '<a href="' . $url . '">' . '<img class="thumbnail" src="' . $url . '"/>' . "</a>" . "</li>";
        }
        echo "</ul>";
        echo "<a class=\"button\" href=\"" . $GLOBALS["conf"]->site_url . "/img_upload.php?" . $path->as_query_only() . "\">Upload image</a>";
        require __DIR__ . "/footer.php";
    }

    public function upload_image(PageData $page)
    {
        require __DIR__ . "/header.php";
        require __DIR__ . "/img_upload.php";
        require __DIR__ . "/footer.php";
    }

    public function error(int $status_code, string $error_msg, \Private\Path $path)
    {
        http_response_code($status_code);
        require __DIR__ . "/header.php";
        require __DIR__ . "/error_page.php";
        require __DIR__ . "/footer.php";
    }
}

$GLOBALS["TEMPLATE"] = new Template();
?>