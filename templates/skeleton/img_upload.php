<form action="<?php echo $GLOBALS["conf"]->site_url . "/img_upload.php?" . $path->as_query_only(); ?>" method="post" enctype="multipart/form-data">
    <label for="fileToUpload">Image to upload</label>
    <input required type="file" accept=".png,.jpg" name="fileToUpload" id="fileToUpload">
    <label for="img_filename_field">
        Filename
    </label>
    <input type="text" name="filename" id="img_filename_field" required pattern="[a-z0-9_]+">
    <input type="submit" value="Carica immagine" name="submit">
</form>