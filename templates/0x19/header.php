<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <link href="<?php echo $GLOBALS["conf"]->get_template_assets_url() . "/default.css" ?>" rel="stylesheet">
</head>
<body>
<nav>
|
    <?php
    foreach ($GLOBALS["breadcrumb"] as $i => $path)
    {
        $cur_class = "";
        if ($i == count($GLOBALS["breadcrumb"]) -1)
        {
            $cur_class = "current";
        }
        echo '<a class="' .  $cur_class . ' navlink" href="' . $path->as_url() . '">' . $path->get_name() . '</a>';
        if ($i != count($GLOBALS["breadcrumb"]) -1)
        {
            echo " &gt; ";
        }
    }
    ?>
|
</nav>
<hr>

<div class="content">