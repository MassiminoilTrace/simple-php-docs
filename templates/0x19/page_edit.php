<form action="<?php echo $path->as_url_mut(); ?>" method="post" accept-charset="utf-8">
    <label for="titolo">Title</label>
    <input required maxlength=20 type="text" name="titolo" id="titolo" value="<?php echo $page->get_title();?>">
    <label for="contenuto">Content</label>
    <textarea required name="contenuto" id="contenuto" cols="100" rows="45"><?php echo $page->get_content_only();?></textarea>
    <button type="submit">Salva</button>
</form>