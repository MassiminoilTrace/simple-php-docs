<h1>Create page or category</h1>

<form action="<?php echo $GLOBALS["conf"]->site_url . "/page_create.php?" . $path->as_query_only(); ?>" method="post">
    <input pattern="[a-zA-Z 0-9]+" type="text" required name="nome">
    <select name="tipo_creazione" required>
        <option value="pagina">Page</option>
        <option value="sottocategoria">Subcategory</option>
    </select>
    <button type="submit">Create</button>
</form>