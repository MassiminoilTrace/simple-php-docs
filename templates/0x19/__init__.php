<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 

require_once __DIR__ . "/../template_load.php";

class Template extends TemplateBase
{
    // Category body
    public function render_category(Path $path)
    {
        $cat = new Category($path);
        $GLOBALS["page_title"] = $cat->get_title();
        require __DIR__ . "/header.php";

        echo "<h1>" . $cat->get_title() . "</h1>";

        foreach ($cat->get_children() as $child)
        {
            $name = $child->get_name();
            $href = $child->as_url();
            if ($child->is_page())
            {$pref = "&gt; ";}
            else
            {$pref = "[cat.] ";}
            echo '<p><a href="' . $href .'">' . $pref . $name . '</a></p>';
        }
        echo "<p style=\"margin-top; 4rem;\"><a href=\"" . $GLOBALS["conf"]->site_url . "/page_create.php?" . $path->as_query_only() . "\"> + Create page or subcategory</a></p>";
        require __DIR__ . "/footer.php";
    }

    // Page body
    public function render_page_view(Path $path)
    {
        $page = new Page($path);
        $GLOBALS["page_title"] = $page->get_title();
        require __DIR__ . "/header.php";

        echo "<h1>" . $page->get_title() . "</h1>";
        echo "<a href=\"" . $path->as_url_mut() . "\">Edit</a>";
        echo "<hr>";

        echo $page->render_content();

        require __DIR__ . "/footer.php";
    }
    public function render_page_edit(Path $path)
    {
        $page = new Page($path);
        require __DIR__ . "/header.php";
        require __DIR__ . "/page_edit.php";
        require __DIR__ . "/footer.php";
    }

    public function render_page_create(Path $path)
    {
        require __DIR__ . "/header.php";
        require __DIR__ . "/page_create.php";
        require __DIR__ . "/footer.php";
    }

    public function index(Path $path){}

    public function list_images(Path $path)
    {
        require __DIR__ . "/header.php";
        echo "<ul>";
        foreach ($path->get_images() as $img)
        {
            $url = $img->as_url() . "&show";
            echo "<li>" . $img->get_filename() . '<a href="' . $url . '">' . '<img class="thumbnail" src="' . $url . '"/>' . "</a>" . "</li>";
        }
        echo "</ul>";
        echo "<a class=\"button\" href=\"" . $GLOBALS["conf"]->site_url . "/img_upload.php?" . $path->as_query_only() . "\">Upload image</a>";
        require __DIR__ . "/footer.php";
    }

    public function upload_image(Path $path)
    {
        require __DIR__ . "/header.php";
        require __DIR__ . "/img_upload.php";
        require __DIR__ . "/footer.php";
    }

    public function error(int $status_code, string $error_msg, Path $path)
    {
        http_response_code($status_code);
        require __DIR__ . "/header.php";
        require __DIR__ . "/error_page.php";
        require __DIR__ . "/footer.php";
    }
}

$GLOBALS["TEMPLATE"] = new Template();
?>