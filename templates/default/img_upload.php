<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
?>
<h1>Carica nuova immagine</h1>
<form action="<?php echo $GLOBALS["conf"]->site_url . "/img_upload.php?" . $page->path->query_string; ?>" method="post" enctype="multipart/form-data">
    <label for="fileToUpload">Immagina da caricare</label>
    <input required type="file" accept=".png,.jpg" name="fileToUpload" id="fileToUpload">

    <label for="img_filename_field">
        Nome file
        <br>
        <small>
            Il nome, tutto minuscolo e senza estensione, con cui verrà salvata l'immagine sul server.
            Se si inserisce un nome file di una immagine già esistente, questa verrà sovrascritta.
        </small>
    </label>
    <input type="text" name="filename" id="img_filename_field" required pattern="[a-z0-9_]+">

    <input type="submit" value="Carica immagine" name="submit">
</form>

<section>
    <header>
        <h4>Immagini già caricate</h4>
    </header>
    <?php require __DIR__ . "/list_images.php";?>
</section>