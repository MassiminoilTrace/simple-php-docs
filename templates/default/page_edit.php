<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
?>

<?php
if (isset($GLOBALS["n_lunr_bytes"]) && $GLOBALS["n_lunr_bytes"]==0)
{
    echo '<div class="notice error">Errore nella generazione dell\'indice di ricerca</div>';
}
?>

<?php
echo "<a href=\"" . $page->path->url . "\">Torna in visualizzazione</a>";
?>

<section>
<form action="<?php echo $page->path->url_mut; ?>" method="post" accept-charset="utf-8">
    <button type="submit">Salva</button>
    <label for="titolo">Titolo</label>
    <input required maxlength=35 type="text" name="titolo" id="titolo" value="<?php echo $page->title;?>">
    <label for="descrizione">Descrizione</label>
    <input required maxlength=300 type="text" name="descrizione" id="descrizione" value="<?php echo $page->description;?>">
    <label for="contenuto">Contenuto</label>
    <textarea required name="contenuto" id="contenuto" cols="100" rows="25"><?php echo $page->content_markdown;?></textarea>
    <button type="submit">Salva</button>
</form>
</section>

<section>
<details>
    <summary>Immagini</summary>
    <header>Caricamento immagini</header>
       <?php require __DIR__ . "/list_images.php"; ?>
</details>
</section>


<link rel="stylesheet" href="<?php echo $GLOBALS["conf"]->get_template_assets_url(); ?>/simplemde/simplemde.min.css">
<script src="<?php echo $GLOBALS["conf"]->get_template_assets_url(); ?>/simplemde/simplemde.min.js"></script>
<script src="<?php echo $GLOBALS["conf"]->get_template_assets_url(); ?>/simplemde/load_simplemde.js"></script>
