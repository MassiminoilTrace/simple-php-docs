<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
?>
<h1>Crea pagina o sottocategoria</h1>

<form action="<?php echo $GLOBALS["conf"]->site_url . "/page_create.php?" . $cat->path->query_string; ?>" method="post">
    <input pattern="[a-zA-Z 0-9]+" type="text" required name="nome">
    <select name="tipo_creazione" required>
        <option value="pagina">Pagina</option>
        <option value="sottocategoria">Sottocategoria</option>
    </select>
    <button type="submit">Crea</button>
</form>