<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.

if (isset($page))
{
  $path = $page->path;
}
else
{
  $path = $cat->path;
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <title><?php echo $GLOBALS["conf"]->site_name . " - " . $title; ?></title>
  <link href="<?php echo $GLOBALS["conf"]->get_template_assets_url() . "/simple.css" ?>" rel="stylesheet">
  <link href="<?php echo $GLOBALS["conf"]->get_template_assets_url() . "/style.css" ?>" rel="stylesheet">
  <link rel="sitemap" type="application/xml" title="Sitemap" href="<?php echo $GLOBALS["conf"]->site_url ?>/sitemap.xml">
  <script src="<?php echo $GLOBALS["conf"]->site_url . "/assets/lunr.js" ?>"></script>
  <?php
  if ( $GLOBALS["conf"]->use_mathjax)
  {
    echo '<script src="' . $GLOBALS["conf"]->site_url . '/assets/configure_mathjax.js"></script>';
    echo '<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"></script>';
  }
  ?>
</head>
<body data-siteurl="<?php echo $GLOBALS["conf"]->site_url; ?>">
<header>
  <img class="logo" src="<?php echo $GLOBALS["conf"]->site_url . "/assets/logo.png"?>" alt="logo">
  <h1><?php echo $GLOBALS["conf"]->site_name; ?></h1>
  <nav>
    <?php
    foreach ($path->breadcrumb as $bread)
    {
      $cur_class = "";
      if ($bread->active)
      {
        $cur_class = "current";
      }
      echo "<a class=\"" . $cur_class . "\" href=\"" . $bread->url . "\">" . $bread->title . "</a>";
    }
    ?>
  </nav>
  <div>
    <label class="search_button button" for="search_button_checkbox">
      <img src="<?php echo $GLOBALS["conf"]->get_template_assets_url() . "/magnifying_glass_icon.svg"?>" alt="">
    </label>
  </div>
</header>
<main>
<input type="checkbox" id="search_button_checkbox">
<div class="modal">
  <div>
    <header>
      <label class="button" for="search_button_checkbox">X</label>
      <input id="id_search_wiki" type="text">
    </header>
    <div>
      <h5>Risultati:</h5> 
      <div id="content_search_results">
        
      </div>
  </div>

  </div>
  
</div>