// Copyright © 2023 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
"use strict";
async function fetch_docs()
{
    return (await fetch(document.body.dataset.siteurl + "/lunr_data.json")).json();
}
(async() =>
    {
        let documents = await fetch_docs();
        
        let idx = lunr(function () {
            this.ref('id');
            this.field('title');
            this.field('description');
            this.field('category_path');
            this.field('content');
            documents.forEach(function (doc, id) {
                doc.id=id;
                this.add(doc)
            }, this)
        });

        function gen_result_html(res)
        {
            let html_data = "";
            for (const item of res)
            {
                let item_data = documents[item.ref];
                html_data += `
                <article class="card">
                <h3>${item_data.title}</h3>
                <p>${item_data.description}</p>
                <p>percorso: ${item_data.category_path}</p>

                <a class="button pagebutton" href="${item_data.url}">Apri</a>
                </article>
                `;
            }
            return html_data;
        }

        document.getElementById("id_search_wiki")
        .addEventListener(
            "input",
            (ev)=>{
                let container = document.getElementById("content_search_results");
                if (ev.target.value.length==0)
                {
                    container.innerHTML = "";
                }
                else
                {
                    let res = idx.search(
                        ev.target.value + "~1"
                    );
                    container.innerHTML = gen_result_html(res);
                }
            }
        );
        
        document.getElementById("search_button_checkbox").addEventListener(
            "change",
            ()=>{
                let search_open_button = document.getElementById("id_search_wiki");
                search_open_button.value = "";
                document.getElementById("content_search_results").innerHTML = "";
            }
        );

        document.addEventListener("keydown", (e)=>{
            if (e.key == "Escape")
            {
                document.getElementById("search_button_checkbox").checked = false;
            }
        });
    }
)();


