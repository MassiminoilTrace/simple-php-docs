"use strict";
let simplemde = new SimpleMDE(
    {
        "blockStyles":{
            "italic": "_"
        },
        "spellChecker": false,
        "forceSync":true
    }
);