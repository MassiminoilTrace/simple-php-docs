<?php namespace Template;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 

require_once __DIR__ . "/../template_load.php";
require_once __DIR__ . "/../template_data.php";

class Template extends TemplateBase
{
    // Category body
    public function render_category(\Template\CategoryData $cat)
    {
        $title = $cat->title;
        require __DIR__ . "/header.php";
        {
            echo "<a class=\"button\" href=\"" . $GLOBALS["conf"]->site_url . "/page_create.php?" . $cat->path->query_string . "\">Crea pagina o categoria</a>";
        }
        foreach ($cat->children as $child)
        {
            $name = $child->get_name();
            $description = $child->get_description();
            $href = $child->as_url();
            if ($child->is_page())
            {
                $button_class = "pagebutton";

            }
            else
            {
                $button_class = "";
            }
        
            echo '<article class="card">';
            echo '<h3>' . $name . '</h3>';
            echo "<p>" . $description . "</p>";
            echo '<a class="button ' . $button_class . '" href="' . $href . '">Apri</a>';
            echo '</article>';
        }
        require __DIR__ . "/footer.php";
    }

    // Page body
    public function render_page_view(\Template\PageData $page)
    {
        $title = $page->title;
        require __DIR__ . "/header.php";

        echo "<h1>" . $page->title . "</h1>";
        echo "<p>" . $page->description . "</p>";
        echo "<a href=\"" . $page->path->url_mut . "\">Modifica</a><br>";
        echo "<a href=\"" . $page->path->url_mut . "\"></a>";
        echo "<hr>";

        echo $page->content_html;

        require __DIR__ . "/footer.php";
    }
    public function render_page_edit(\Template\PageData $page)
    {
        $title = "Modifica pagina " . $page->title;
        require __DIR__ . "/header.php";
        require __DIR__ . "/page_edit.php";
        require __DIR__ . "/footer.php";
    }

    public function render_page_create(\Template\CategoryData $cat)
    {
        $title = "Modifica pagina " . $cat->title;
        require __DIR__ . "/header.php";
        require __DIR__ . "/page_create.php";
        require __DIR__ . "/footer.php";
    }

    public function index(Private\Path $path){}

    public function list_images(\Template\PageData $page)
    {
        require __DIR__ . "/header.php";
        require __DIR__ . "/list_images.php";
        require __DIR__ . "/footer.php";
    }

    public function upload_image(\Template\PageData $page)
    {
        $title = "Carica immagine";
        require __DIR__ . "/header.php";
        require __DIR__ . "/img_upload.php";
        require __DIR__ . "/footer.php";
    }

    public function error(int $status_code, string $error_msg, \Private\Path $path)
    {
        http_response_code($status_code);
        $GLOBALS["page_title"] = "Errore";
        require __DIR__ . "/header.php";
        require __DIR__ . "/error_page.php";
        require __DIR__ . "/footer.php";
    }
}

$GLOBALS["TEMPLATE"] = new Template();
?>