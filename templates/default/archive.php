<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
if (file_exists(\Private\ARCHIVE_TARGZ_PATH))
{
    $file_seconds = filemtime(\Private\ARCHIVE_TARGZ_PATH);
    echo "<p>";
    echo "<a href=\"";
    echo $GLOBALS["conf"]->site_url . "/archive.php?download";
    echo "\">Scarica archivio del sito 💾</a>";
    echo "<br>";
    echo "<small>Generato il " . date("j/m/Y", $file_seconds) . " alle " . date("H:i:s", $file_seconds) . "</small>";
    echo "</p>";
}
?>