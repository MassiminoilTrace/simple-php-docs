<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
echo "<ul>";
foreach ($page->images as $img)
{
    $url = $img->as_url() . "&show";
    echo "<li>" . $img->get_filename() . '<a href="' . $url . '">' . '<img class="thumbnail" src="' . $url . '"/>' . "</a>" . "</li>";
}
echo "</ul>";
echo "<a class=\"button\" href=\"" . $GLOBALS["conf"]->site_url . "/img_upload.php?" . $page->path->query_string . "\">" . "Carica nuova immagine" . "</a>";
?>