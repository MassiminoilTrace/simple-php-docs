<?php namespace Template;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
require_once __DIR__ . "/../private/path.php";
require_once __DIR__ . "/../private/category.php";
require_once __DIR__ . "/../private/page.php";
require_once __DIR__ . "/template_data.php";

abstract class TemplateBase
{
    // Category body
    abstract public function render_category(CategoryData $cat);

    // Page body
    abstract public function render_page_view(PageData $page);
    abstract public function render_page_edit(PageData $path);
    abstract public function render_page_create(CategoryData $cat);

    public abstract function list_images(PageData $page);
    public abstract function upload_image(PageData $page);

    // Errors
    public abstract function error(int $status_code, string $error_msg, \Private\Path $path);
}

require_once $GLOBALS["conf"]->template . "/__init__.php";

?>