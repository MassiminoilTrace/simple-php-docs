<?php namespace Template;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
require_once __DIR__ . "/../private/path.php";
require_once __DIR__ . "/../private/category.php";
require_once __DIR__ . "/../private/page.php";

class PathData
{
    public readonly array $folders;
    public readonly bool $is_category;
    public readonly bool $is_page;
    public readonly string $url;
    public readonly string $url_mut;
    public readonly string $query_string;

    // array<BreadcrumbEntry>
    public readonly array $breadcrumb;

    public function __construct(\Private\Path $path)
    {
        $this->folders = $path->as_folder_array();
        $this->is_category = $path->is_category();
        $this->is_page = $path->is_page();
        $this->url = $path->as_url();
        $this->url_mut = $path->as_url_mut();
        $this->query_string = $path->as_query_only();
        $this->breadcrumb = $path->get_breadcrumb_data();
    }
}

class PageData
{
    public readonly PathData $path;
    public readonly string $title;
    public readonly string $description;
    public readonly string $content_markdown;
    public readonly string $content_html;
    public readonly array $images;

    public function __construct(\Private\Path $path)
    {
        $this->path = new PathData($path);
        if (!$this->path->is_page)
        {
            throw new \Exception('Impossibile creare dati pagina a partire da un percorso che non è una pagina');
        }
        $page = new \Private\Page($path);
        
        $this->title = $page->get_title();
        $this->description = $page->get_description();
        $this->content_markdown = $page->get_content_only();
        $this->content_html = $page->render_content();
        $this->images = $path->get_images();
    }
}


class CategoryData
{
    public readonly PathData $path;
    public readonly string $title;
    public readonly array $children;

    public function __construct(\Private\Path $path)
    {
        $this->path = new PathData($path);
        if (!$this->path->is_category)
        {
            throw new \Exception('Impossibile creare dati categoria a partire da un percorso che non è una categoria');
        }
        $cat = new \Private\Category($path);
        
        $this->title = $cat->get_title();
        $this->children = $cat->get_children();
    }
}
?>