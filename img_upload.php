<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
require_once "private/CONST.php";
require_once "private/path.php";
require_once "private/image.php";
require_once "private/configuration.php";
require_once __DIR__ . "/templates/template_load.php";

$p = new \Private\Path();
$p->build_from_query_param($_GET["path"]);
if (
    $_SERVER["REQUEST_METHOD"] == "POST"
    && isset($_POST["submit"])
    && isset($_POST["filename"])
    && isset($_FILES["fileToUpload"])
    )
{
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if ($check)
    {
        if (str_ends_with($_FILES["fileToUpload"]["name"], ".jpg"))
        {
            $estensione = ".jpg";
        }
        else if (str_ends_with($_FILES["fileToUpload"]["name"], ".png"))
        {
            $estensione = ".png";
        }
        else
        {
            http_response_code(403);
            echo "Estensione non supportata";
            die;
        }
        $img = new \Private\Image($p, $_POST["filename"] . $estensione);
        $img->save_new($_FILES["fileToUpload"]["tmp_name"]);
        header("Location: " . $GLOBALS["conf"]->site_url . "/img.php?" . $p->as_query_only());
        exit;
    }
    else
    {
        http_response_code(403);
        echo "Dati immagine non validi";
        die;
    }
}

$GLOBALS["TEMPLATE"]->upload_image(
    new \Template\PageData($p)
);
?>
