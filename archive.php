<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
require_once "private/configuration.php";
require_once "private/CONST.php";

$tar_gz_exists = file_exists(\Private\ARCHIVE_TARGZ_PATH);
if (isset($_GET["download"]))
{
    if ($tar_gz_exists)
    {
        $filename_downloadable_file = date("Ymd_H_i_s", filemtime(\Private\ARCHIVE_TARGZ_PATH)) . "_archive.tar.gz";
        header("Content-Type: application/gzip");
        header("Content-Disposition: attachment; filename=\"" . $filename_downloadable_file . "\"");
        header("Content-Length: " . filesize(\Private\ARCHIVE_TARGZ_PATH));
        readfile(\Private\ARCHIVE_TARGZ_PATH);
    }
    else
    {
        http_response_code(404);
        echo "File archivio non trovato";
    }
}

?>