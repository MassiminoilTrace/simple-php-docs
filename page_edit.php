<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>. 
require_once "private/path.php";
require_once "private/page.php";
require_once "private/CONST.php";
require_once "private/configuration.php";
require_once __DIR__ . "/templates/template_load.php";

$p = new \Private\Path();
$p->build_from_query_param($_GET["path"]);
$GLOBALS["breadcrumb"] = $p->get_breadcrumb_data();
if (!$p->is_page())
{
    echo "not a page";
    die;
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if (isset($_POST["titolo"]) && isset($_POST["descrizione"]) && isset($_POST["contenuto"]))
    {
        $page = new \Private\Page($p);
        $page->save_edit(
            $_POST["titolo"],
            $_POST["descrizione"],
            $_POST["contenuto"]
        );
    }
}

$GLOBALS["TEMPLATE"]->render_page_edit(
    new \Template\PageData($p)
);
?>