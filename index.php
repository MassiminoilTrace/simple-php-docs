<?php
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
include "private/path.php";
require_once "private/category.php";
require_once "private/page.php";
require_once "private/CONST.php";
require_once "private/configuration.php";

require_once "private/generate_archive.php";
require_once __DIR__ . "/templates/template_load.php";

$p = new Private\Path();
$p->build_from_query_param(
    isset($_GET["path"]) ? $_GET["path"] : ""
);
$GLOBALS["breadcrumb"] = $p->get_breadcrumb_data();
if ($p->is_category())
{
    $GLOBALS["TEMPLATE"]->render_category(
        new \Template\CategoryData($p)
    );
}
else if ($p->is_page())
{
    $GLOBALS["TEMPLATE"]->render_page_view(
        new \Template\PageData($p)
    );
}
else
{
    $GLOBALS["TEMPLATE"]->error(
        404,
        "Percorso \"" . $p->as_url() . "\" inesistente",
        $p
    );
}
?>
