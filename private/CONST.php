<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.

const REGEXP_TITLE_FILTER = "/[^a-zA-Z0-9 _'àèéìòùAÈÉÌÒÙ]/u";
const REGEXP_TITLE_EXTRACT = "/title\s*=\s*\"([a-zA-Z0-9 _'àèéìòùAÈÉÌÒÙ]+)\"/u";
//const REGEXP_CONTENT_FILTER = "/[^a-zA-Z0-9_\"` '\\-\n|àèéìòùAÈÉÌÒÙ?!@+*#\[\]\.\(\):,;\/€\$]/u";
const REGEXP_CONTENT_FILTER = "/[<]/u";

const REGEXP_DESCRIPTION_FILTER = "/[^a-zA-Z0-9 _'àèéìòùAÈÉÌÒÙ]/u";
const REGEXP_DESCRIPTION_EXTRACT = "/description\s*=\s*\"([a-zA-Z0-9 _'àèéìòùAÈÉÌÒÙ]+)\"/u";

const REGEXP_IMG_NAME_EXTRACT = "/^([a-z0-9_]+)\.(png|jpg)$/u";
const REGEXP_PATH_QUERYSTRING = "/[^a-z0-9_|?]/u";

const REGEXP_RELATIVE_IMAGE_LINK = "/!\[([0-9a-zA-Z _,\.àèéìòù']*)\]\(([a-z0-9_]+\.(png|jpg))\)/u";
const REGEXP_RELATIVE_PAGE = "/\[([0-9a-zA-Z _,\.àèéìòù']*)\]\(([a-z0-9_|]+)\)/u";

const ARCHIVE_TARGZ_PATH = __DIR__ . "/../cache/archive.tar.gz";
const ARCHIVE_TAR_PATH = __DIR__ . "/../cache/archive.tar";
// Un file la cui data di modifica è la data di modifica di pagine e immagini
// nella cartella contenuti. Eseguire touch() per aggiornarla
const LAST_MODIFICATION_PATH = __DIR__ . "/../cache/last_modification";
if (!file_exists(LAST_MODIFICATION_PATH))
{
    touch(LAST_MODIFICATION_PATH);
}

const SITEMAP_PATH = __DIR__ . "/../sitemap.xml";
if (!file_exists(SITEMAP_PATH))
{
    require __DIR__ . "/sitemap.php";
}
?>