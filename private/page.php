<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
require_once  __DIR__ . '/../parsedown-1.7.4/Parsedown.php';
require_once  __DIR__ . '/../ParsedownToc/src/ParsedownToc.php';
require_once "path.php";
require_once "CONST.php";
class Page
{
    private $path;

    function __construct(Path $path_obj)
    {
       $this->path = $path_obj;
    }

    public function save_edit(string $titolo, string $descrizione, string $contenuto)
    {
        $titolo = preg_replace(REGEXP_TITLE_FILTER, "", $titolo);
        //$contenuto = preg_replace(REGEXP_CONTENT_FILTER, "", $contenuto);

        file_put_contents(
            $this->path->as_md_file(),
            "+++\ntitle=\"" . $titolo . "\"\n" . 
            "description=\""  . $descrizione  .  "\"\n" .
            "+++\n" .
            $contenuto
        );
        require_once "lunr_index.php";
        $lunr = new LunrJson();
        $lunr->regenerate_file();
        touch(LAST_MODIFICATION_PATH);
    }

    public function render_content() : string
    {
        if ($this->path->is_page())
        {
            $content = $this->insert_relative_links(
                $this->get_content_only()
            );
            $ParsedownToc = new \ParsedownToc();

            $body = $ParsedownToc->body($content);
            $toc  = $ParsedownToc->contentsList();
            return $toc
                . "<hr>"
                . $body;
        }
        else
        {
            http_response_code(503);
            echo "Questa non è una pagina";
            die;
        }
    }

    public function get_full_file_content() : string
    {
        return file_get_contents($this->path->as_md_file());
    }

    public function get_content_only() : string
    {
        $s = file_get_contents($this->path->as_md_file());

        return substr(
            $s,
            strpos($s, "+++", 1) + 4
        ); 
    }

    function isolate_frontmatter() : String
    {
        $full_text = $this->get_full_file_content();
        $frontmatter_start_index = strpos($full_text, "+++")+3;
        if ($frontmatter_start_index==false)
        {
            http_response_code(503);
            echo "Frontmatter non valido";
            die;
        }
        $frontmatter_stop_index = strpos($full_text, "+++", $frontmatter_start_index);
        if ($frontmatter_stop_index==false)
        {
            http_response_code(503);
            echo "Frontmatter non valido";
            die;
        }

        $frontmatter = substr($full_text, $frontmatter_start_index, $frontmatter_stop_index-$frontmatter_start_index);
        return $frontmatter;
    }

    public function get_title() : string
    {
        $lines = explode("\n", $this->isolate_frontmatter());
        foreach ($lines as $i => $line)
        {
            preg_match(REGEXP_TITLE_EXTRACT, $line, $output);
            if (count($output)>=2)
            {
                return $output[1];
            }
        }
        return "";
    }

    public function get_description() : ?string
    {
        $lines = explode("\n", $this->isolate_frontmatter());
        foreach ($lines as $i => $line)
        {
            preg_match(REGEXP_DESCRIPTION_EXTRACT, $line, $output);
            if (count($output)>=2)
            {
                return $output[1];
            }
        }
        return "";
    }

    function insert_relative_links(string $text_content) : string
    {
        // Immagini
        $text_content = preg_replace_callback(
            REGEXP_RELATIVE_IMAGE_LINK,
            function ($a)
            {
                $img = new Image($this->path, $a[2]);
                $res = "![" . $a[1] . "](" . $img->as_url() .")";
                // print_r($res);
                return $res;
            },
            $text_content
        );

        // Altre pagine
        $text_content = preg_replace_callback(
            REGEXP_RELATIVE_PAGE,
            function ($a)
            {
                $p = new Path();
                $p->build_from_query_param($a[2]);
                if (strlen($a[1]>0))
                {
                    $titolo_link = $a[1];
                }
                else
                {
                    if ($p->is_page()||$p->is_category())
                    {
                        $titolo_link = $p->get_name();
                    }
                    else
                    {
                        $titolo_link = "PERCORSO " . $a[1] . " NON TROVATO!";
                    }
                }
                $res = "[" . $a[1] . "](" . $p->as_url() .")";
                return $res;
            },
            $text_content
        );
        
        return $text_content;
    }

    public function get_last_edit_timestamp(): int|false
    {
        return filemtime(
            $this->path->as_md_file()
        );
    }
}
?>
