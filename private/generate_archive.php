<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.

if (file_exists(ARCHIVE_TAR_PATH))
{
    unlink(ARCHIVE_TAR_PATH);
}
if (
    $GLOBALS["conf"]->archive_interval>=0
    &&
    (
        !file_exists(ARCHIVE_TARGZ_PATH)
        ||
        (
            time() - filemtime(ARCHIVE_TARGZ_PATH) > $GLOBALS["conf"]->archive_interval
            &&
            filemtime(LAST_MODIFICATION_PATH) > filemtime(ARCHIVE_TARGZ_PATH)
        )
    )
)
{
    $phar = new \PharData(ARCHIVE_TAR_PATH);
    $phar->buildFromDirectory(__DIR__ . '/../content');
    unlink(ARCHIVE_TARGZ_PATH);
    $phar->compress(\Phar::GZ);
    unlink(ARCHIVE_TAR_PATH);
}
?>