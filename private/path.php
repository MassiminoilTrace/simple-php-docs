<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
require_once "image.php";
require_once "page.php";
require_once "CONST.php";
require_once "configuration.php";

// Funzione comoda per estrarre la querystring
// Restituisce l'array spezzettato del percorso
function sanitize_address(string $path) : array
{
    if (is_null($path) || $path == "")
    {
        return [];
    }
    $path = explode(
        "|",
        preg_replace(REGEXP_PATH_QUERYSTRING, "", $path),
        5
    );
    return $path;
}

class BreadcrumbEntry
{
    public readonly string $title;
    public readonly string $url;
    public readonly bool $active;

    public function __construct(\Private\Path $path, bool $active=false)
    {
        $this->title = $path->get_name();
        $this->url = $path->as_url();
        $this->active = $active;
    }
}

class Path
{
    // Per esempio "abc|def" diventa ["abc", "def"]
    private $folders_array = [];
    // Per esempio "abc|def" diventa "content/abc/def"
    private $file_path_folder = "content";

    public function build_from_query_param(?string $query_param_path)
    {
        if ( is_null($query_param_path))
        {
            $query_param_path = "";
        }
        // $this->folders_array = array_filter(
        //     sanitize_address($query_param_path),
        //     function ($v){return strlen($v) > 0;}
        // );
        $this->build_from_array(
            sanitize_address($query_param_path),
            function ($v){return strlen($v) > 0;}
        );
        
        // $this->file_path_folder = __DIR__ . "/../" . implode(
        //     "/",
        //     array_merge(["content"], $this->folders_array)
        // );
        // echo $this->file_path_folder;
    }

    function build_from_array(array $folders)
    {
        $this->folders_array = $folders;
        for ($i=0; $i<count($folders); $i++)
        {
            $this->folders_array[$i] = preg_replace(REGEXP_PATH_QUERYSTRING, "", $this->folders_array[$i]);
        }
        $this->file_path_folder = __DIR__ . "/../" . implode(
            "/",
            array_merge(["content"], $this->folders_array)
        );
    }

    public function as_md_file() : string
    {
        return $this->file_path_folder . "/index.md";
    }

    public function as_folder_path() : string
    {
        return $this->file_path_folder;
    }
    public function as_folder_array() : array
    {
        return $this->folders_array;
    }


    public function is_category() : bool
    {
        return (
            is_dir($this->file_path_folder) && !is_file($this->as_md_file())
        );
    }

    public function is_page() : bool
    {
        return is_file($this->as_md_file());
    }

    public function find_children() : array
    {
        $subcategories = [];
        $fs_iterator = new \FileSystemIterator($this->file_path_folder);
        foreach ($fs_iterator as $file)
        {
            if (!is_dir(
                $file
            ))
            {
                continue;
            }
            $p = new Path();
            $p->build_from_array(
                array_merge($this->folders_array, [$file->getBaseName()])
            );
            array_push($subcategories, $p);
        }
        uasort(
            $subcategories,
            function(Path $a, Path $b)
            {
                return $a->get_name() > $b->get_name();
            }
        );
        return $subcategories;
    }

    public function _get_page_data_recursive() : array
    {
        $collected_data = [];
        foreach ($this->find_children() as $item)
        {
            if ($item->is_category())
            {
                $collected_data = array_merge(
                    $collected_data,
                    $item->_get_page_data_recursive()
                );                
            }
            else
            {
                
                $p = new Page($item);
                array_push(
                    $collected_data,
                    array(
                        "title" => $p->get_title(),
                        "description" => $p->get_description(),
                        "url" => $item->as_url(),
                        "category_path" => implode(" - ",$this->folders_array),
                        "content" => $p->get_content_only(),
                        "date" => date(
                            "Y-m-d",
                            $p->get_last_edit_timestamp()
                        )
                    )
                );   
            }
        }
        return $collected_data;
    }
    
    public function as_url() : string
    {
        return $GLOBALS["conf"]->site_url . "/index.php?" . $this->as_query_only();
    }

    public function as_url_mut() : string
    {
        return $GLOBALS["conf"]->site_url . "/page_edit.php?" . $this->as_query_only();
    }

    public function as_query_only() : string
    {
        return "path=" . urlencode(implode("|", $this->folders_array ));
    }

    public function get_name() : string
    {
        if (count($this->folders_array) == 0)
        {
            return "Home";
        }
        else
        {
            if ($this->is_page())
            {
                $p = new Page($this);
                // $p->build_from_array($this->folders_array);
                return $p->get_title();
            }
            else
            {
                return preg_replace(
                    "/_/",
                    " ",
                    $this->folders_array[count($this->folders_array)-1]
                );
            }
        }
    }

    public function get_description() : string
    {
        if ($this->is_page())
        {
            $p = new Page($this);
            // $p->build_from_array($this->folders_array);
            return $p->get_description();
        }
        else
        {
            return "";
        }
    }

    public function get_breadcrumb_data() : array
    {
        $ar = array();
        for ($i=0; $i<count($this->folders_array)+1; $i++)
        {
            $p = new Path();
            $p->build_from_array(
                array_slice(
                    $this->folders_array,
                    0,
                    count($this->folders_array)-$i
                )
            );
            array_push(
                $ar,
                new BreadcrumbEntry(
                    $p,
                    ($i==0)
                )
            );
        }
        $ar = array_reverse($ar);
        return $ar;
    }

    public function create(string $nome, string $nome_cartella, bool $is_page) : Path
    {
        $p = new Path();
        $p->build_from_array(
            array_merge($this->folders_array, [$nome_cartella])
        );
        if (!$this->is_page() && !is_dir($p->as_folder_path()))
        {
            mkdir($this->file_path_folder . "/" . $nome_cartella);
            if ($is_page)
            {
                $page = new Page($p);
                $page->save_edit($nome, "", "");
                require_once "sitemap.php";
            }
            touch(LAST_MODIFICATION_PATH);
            return $p;
        }
        else
        {
            http_response_code(403);
            echo "Impossibile creare contenuti dentro a una pagina";
            require "footer.php";
            die;
        }
    }

    // Restituisce array<Image>
    public function get_images() : array
    {
        if (!$this->is_page())
        {
            http_response_code(403);
            echo "Il percorso non può ottenere immagini";
            die;
        }

        $images = [];
        $fs_iterator = new \FileSystemIterator($this->file_path_folder);
        foreach ($fs_iterator as $file)
        {
            if (
                preg_match(
                    REGEXP_IMG_NAME_EXTRACT,
                    $file->getBaseName()
                )
            )
            {
                $img = new Image($this, $file->getBaseName());
                array_push($images, $img);
            }
        }
        return $images;
    }
} ?>