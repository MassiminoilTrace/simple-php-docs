<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
require_once "path.php";
require_once "CONST.php";
require_once "configuration.php";

class Image
{
    private Path $path;
    private string $nome;
    private string $estensione;

    function __construct(Path $path, ?string $nome_img)
    {
        if (!isset($nome_img))
        {
            http_response_code(403);
            echo "Percorso vuoto";
            die;
        }
        if (!$path->is_page())
        {
            http_response_code(403);
            echo "Il percorso non può ottenere immagini";
            die;
        }
        preg_match(
            REGEXP_IMG_NAME_EXTRACT,
            $nome_img,
            $matches
        );
        if (isset($matches) && count($matches) != 3)
        {
            http_response_code(403);
            echo "Formattazione nome img errato";
            die;
        }
        $this->path = $path;
        $this->nome = $matches[1];
        $this->estensione = $matches[2];
    }

    public function save_new($tmp_path)
    {
        move_uploaded_file(
            $tmp_path,
            $this->as_full_file_path()
        );
        touch(LAST_MODIFICATION_PATH);
    }

    function as_full_file_path() : string
    {
        $img_path = $this->path->as_folder_path() . "/" . $this->nome . "." . $this->estensione;
        return $img_path;
    }

    function get_filename() : string
    {
        return $this->nome . "." . $this->estensione;
    }

    public function dump()
    {
        if ($this->estensione=="png")
        {
            $mime = "image/png";
        }
        else
        {
            $mime = "image/jpg";
        }
        
        $img_path = $this->as_full_file_path();
        if (!file_exists($img_path))
        {
            http_response_code(404);
            echo "Img non esiste ";
            die;
        }
        header("Content-Type: " . $mime);
        header("Content-Length: " . filesize($img_path));
        readfile($img_path);
    }

    public function as_url() : string
    {
        return $GLOBALS["conf"]->site_url . "/img.php?" . $this->path->as_query_only() . "&img=" . urlencode(
            $this->nome . "." . $this->estensione
        );
    }
}
?>