<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
require_once  __DIR__ . '/../parsedown-1.7.4/Parsedown.php';

require_once "path.php";
require_once "CONST.php";
require_once "configuration.php";

class Category
{
    private $path;

    function __construct(Path $path_obj)
    {
       $this->path = $path_obj;
    }

    public function get_children() : array
    {
        // echo $this->subcategories;
        $children = $this->path->find_children();

        $sorted_children = array_merge(
            array_filter(
                $children,
                function($el) {return $el->is_category();}
            ),
            array_filter(
                $children,
                function($el) {return $el->is_page();}
            )
        );
        return $sorted_children;        
    }

    public function get_title() : string
    {
        return "Categoria " . $this->path->get_name();
    }
}
?>