<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.

require_once "path.php";
require_once "page.php";
$path = new Path();
$path->build_from_array([]);
$pages = $path->_get_page_data_recursive();

$content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$content .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
foreach ($pages as $page)
{
    $content .= "    <url>\n";
    $content .= "        <loc>https://" . $_SERVER['SERVER_NAME'] . $page["url"] . "</loc>\n";
    $content .= "        <lastmod>" . $page["date"] . "</lastmod>\n";
    $content .= "    </url>\n";
}
$content .= "</urlset>";

$out_path = __DIR__ . "/../" . "sitemap.xml";
file_put_contents(
    $out_path,
    $content
);
?>
