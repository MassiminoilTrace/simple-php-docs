<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
require_once "path.php";

class LunrJson
{
    function lunr_json_generate() : string
    {
        $path = new Path();
        $path->build_from_array([]);
        $found_data = $path->_get_page_data_recursive();

        return json_encode(
            $found_data
        );
    }
    public function write_to_stdout()
    {
        header('Content-Type: application/json');
        echo $this->lunr_json_generate();
    }

    public function regenerate_file()
    {
        $out_path = __DIR__ . "/../" . "lunr_data.json";
        $data = $this->lunr_json_generate();
        $GLOBALS["n_lunr_bytes"] = file_put_contents(
            $out_path,
            $data
        );
    }
}
?>