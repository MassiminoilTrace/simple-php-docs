<?php namespace Private;
// Copyright © 2023,2024 Massimo Gismondi
//
// This file is part of SimplePHPdocs.
// 
// SimplePHPdocs is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
// 
// SimplePHPdocs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License along with SimplePHPdocs.
// If not, see <https://www.gnu.org/licenses/>.
class SiteConfiguration
{
    public readonly string $site_name;
    public readonly string $site_url;
    public readonly bool $use_mathjax;
    // Ogni quanti secondi va rigenerato un nuovo archivio dei contenuti
    public readonly int $archive_interval;
    public readonly string $template;

    function __construct()
    {
        $rows = explode(
            "\n",
            file_get_contents(
                __DIR__ . '/../site_config/config.toml'
            )
        );
        foreach ($rows as $row)
        {
            if (strlen($row)>0 && $row[0]== "#")
            {
                // Commento
                continue;
            }
            // Sitename
            preg_match(
                "/site_name *= *\"([^\"\n]*)\"/u",
                $row,
                $output);
            if (count($output)==2)
            {
                $this->site_name = $output[1];
                continue;
            }

            // Site URL
            preg_match(
                "/site_url *= \"([a-z0-9\/\-_]*)\"/u",
                $row,
                $output);
            if (count($output)==2)
            {
                $this->site_url = $output[1];
                continue;
            }
            // Use MathJax
            preg_match(
                "/use_mathjax *=[ \"]*(True|TRUE|true|1)[ \"]*/u",
                $row,
                $output);
            if (count($output)==2)
            {
                $this->use_mathjax = true;
                continue;
            }

            //archive_interval
            preg_match(
                "/archive_interval *= *([0-9-]+) */u",
                $row,
                $output);
            if (count($output)==2)
            {
                $this->archive_interval = intval($output[1]);
                continue;
            }

            // template
            preg_match(
                "/template *=[ \"]*([a-z0-9\-_]*)\"*/u",
                $row,
                $output);
            if (count($output)==2)
            {
                $this->template = $output[1];
                continue;
            }
        }

        // Valori default
        if (!isset($this->site_name)) {$this->site_name = "Wiki";}
        if (!isset($this->site_url)) {$this->site_url = "";}
        if (!isset($this->use_mathjax)) {$this->use_mathjax = false;}
        // valore default è un giorno
        if (!isset($this->archive_interval)) {$this->archive_interval = 86400;}
        if (!isset($this->template)) {$this->template = "default";}
    }

    public function get_template_assets_url() : string
    {
        return $this->site_url . "/templates/" . $this->template . "/assets";
    }
}

if (!isset($GLOBALS["conf"]))
{
    $GLOBALS["conf"] = new SiteConfiguration();
    require_once __DIR__ . "/../templates/template_load.php";
}
?>