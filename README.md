# Simple PHP docs

Una comoda wiki organizzata per file e cartelle, inizialmente sviluppata nel tempo libero per Fablab Imperia.

- ✅ no database, tutto salvato su cartelle e normali file di testo
- ✅ sintassi formato markdown
- ✅ editor web delle pagine
- ✅ intralink
- ✅ ricerca testuale _client-side_
- ✅ generazione automatica _sitemap_
- ✅ backup e trasferimento sito velocissimi, è sufficiente copiare i file
- ✅ aggiornamento software tramite Git
- ✅ autenticazione tramite `.htpasswd`
- ✅ archivio contenuti scaricabile come `tar.gz`

SimplePHPDocs punta a non _reinventare la ruota_. Funziona su un qualsiasi servizio di hosting condiviso, è scritta in PHP per la massima compatibilità software; per l'autenticazione usa il sistema già esistente in Apache tramite file `.htpasswd`.


# Installazione
Copiare tutto il contenuto del repository nella cartella desiderata sul server.

Il repository fornisce già alcuni file `.htaccess` nelle cartelle `content` e `private`, per impedire l'accesso.
Bisogna aggiungere un altro file `.htaccess` e `.htpasswd` per restringere l'accesso in modifica.
Generare il file con gli hash `.htpasswd` usando lo strumento da linea di comando htpasswd, assicurandosi di adoperare algoritmi sicuri.
Per esempio, per usare BCrypt:
`htpasswd -B -n nome_utente`

Esempio `.htaccess`:
```apache_conf
Header set Referrer-Policy "no-referrer"
Header set Permissions-Policy "accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=()"
Header set Content-Security-Policy "script-src 'self'"
Header set Strict-Transport-Security "max-age=31536000; includeSubDomains"
Header set X-Frame-Options "DENY"
Header set X-Content-Type-Options "nosniff"

<FilesMatch "^(img_upload\.php|page_create\.php|page_edit\.php)$">
AuthType basic
AuthName "zona admin"
AuthUserFile percorso_assoluto_del_file_htpasswd
Require valid-user
</FilesMatch>

// Blocca cartella .git, se presente
RedirectMatch 404 /\.git
```

Suggerisco di configurare il file `robots.txt` per bloccare i crawler di GPT
```
User-agent: *
Allow: /
User-agent: GPTBot
Disallow: /
```

## Configurazione

Caricare il proprio logo, e porlo in `assets/logo.png`.

Creare un file di testo `site_config/config.toml` in cui impostare il nome del sito, il percorso a cui si trova e altre impostazioni. Ecco un esempio modificabile:
```toml
# Il nome del sito che comparirà nella pagina web
site_name = "My Wiki"

# La wiki si trova in una sottocartella della webroot? Configurarla come sotto.
# Se invece si trova nella webroot,
# rimuovere questo campo o lasciarlo impostato a una stringa vuota
site_url = "/subfolder_name"

# Mathjax serve per mostrare formule matematiche scritte
# con sintassi LaTeX
use_mathjax = true

# Ogni quanti secondi deve essere rigenerato l'archivio
# Impostarlo a un numero negativo, per esempio -1, per non generarlo mai.
# Valore predefinito a 86400, ossia 24 ore
archive_interval = 86400

# Il nome del template da usare. Il predefinito è chiamato `default`
template = "default"
```

Per quanto riguarda l'archivio dei dati, viene generato il file `cache/archive.tar.gz` nella cartella principale di simplephpdocs.
Se si desidera, si può gestire l'archiviazione tramite un cron job esterno (da configurare manualmente) anziché da PHP, disabilitando la impostando `archive_interval = -1`.
Se il file `cache/archive.tar.gz` esiste, indipendentemente da come è stato generato, verrà mostrato un link per scaricarlo.


## Template

Il sito è preconfigurato con un'impaginazione basata su simpleCss.
Tuttavia, se ne possono aggiungere altri nella cartella `templates`. Modificare il file di configurazione opportunamente.
Se il template esterno è versionato tramite `git`, è possibile aggiugerlo con `git submodule`.

Il software integra già tre modelli:
- `default`: modello basato su SimpleCSS predefinito, se non viene specificato altrimenti;
- `skeleton`: utile solo per lo sviluppo, come base da cui partire per sviluppare nuovi temi;
- `0x19`: tema ultra minimale ispirato da [0x19.org](https://0x19.org/).


## Aggiornamento

Se il webserver in uso fornisce il comando `git`, è sufficiente eseguire `git pull`. Il file `.gitignore` di questo repository è stato creato in modo da ignorare la cartella contenuti dell'utente, i file di indice generati, sitemap e le configurazioni utente.
In questo modo, `git pull` andrà a modificare solo i file unicamente interni al software.


# Uso
È possibile modificare gli articoli in formato Markdown direttamente dall'interfaccia web. Prontuario sull'uso di markdown [qui](https://www.markdownguide.org/cheat-sheet/).

## Link interni e immagini
Per inserire link interni ad altre pagine interne oppure a immagini caricate dall'interfaccia web, usare queste scorciatoie.

```markdown
// Per immagini caricate nella pagina attuale
![testo alternativo](nome file immagine)

// Per intra link ad altra pagine della wiki
[testo mostrato](percorso pagina interna, separato da pipe "|")
// esempio
[testo](abc|categoria2|nomepaginatest)
```


# Note copyright

Software pubblicato con licenza AGPL, versione 3 o successiva. Di seguito i dettagli:

This is NOT public domain, make sure to respect the license terms.

You can find the license text in the COPYING file.

Copyright © 2023 Massimo Gismondi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.



## Dipendenze/Dependencies
Questo software utilizza alcune dipendenze esterne, di cui si riportano le note di copyright:

- foglio di stile [SimpleCSS](https://simplecss.org/) nel file `assets/simple.css` è stato realizzato da Kev Quirk, pubblicato con licenza MIT(expat)
- libreria [Parsedown](https://github.com/erusev/parsedown), i cui file sono inclusi nella cartella "parsedown-1.7.4". La libreria Parsedown è pubblicata con licenza MIT(expat), reperibile sul suo repository ufficiale come da link precedente
- libreria [ParsedownExtended](https://github.com/BenjaminHoegh/ParsedownExtended), copyright 2021 the ParsedownExtended Authors, MIT (expat) license
- libreria [lunrjs](https://lunrjs.com/), nel file `assets/lunr.js`, licenza MIT(expat), Copyright © 2020 Oliver Nightingale
- icona lente d'ingrandimento, pubblico dominio, creata da [Deferman e Sarang](https://commons.wikimedia.org/wiki/File:Magnifying_glass_icon.svg)
- libreria [SimpleMDE](https://simplemde.com/), licenza MIT(expat), Copyright © 2015 Next Step Webs Inc, contenuta nella cartella `/assets/simplemde`